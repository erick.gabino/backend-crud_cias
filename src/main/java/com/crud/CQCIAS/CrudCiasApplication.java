package com.crud.CQCIAS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudCiasApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudCiasApplication.class, args);
	}

}

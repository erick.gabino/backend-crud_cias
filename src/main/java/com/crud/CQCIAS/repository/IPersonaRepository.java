package com.crud.CQCIAS.repository;

import java.util.List;
import com.crud.CQCIAS.model.PersonaModel;

public interface IPersonaRepository {
	public List<PersonaModel> findAll();
	public int save(PersonaModel personaModel);
	public int update(PersonaModel personaModel, int id);
	public int deleteById(int id);
}

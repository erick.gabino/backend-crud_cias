package com.crud.CQCIAS.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.crud.CQCIAS.model.PersonaModel;

@Repository
public class PersonaRepository implements IPersonaRepository{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Override
	public List<PersonaModel> findAll() {
		String SQL = "SELECT * FROM Persona WHERE estatus = 'A'";
		return jdbcTemplate.query(SQL, BeanPropertyRowMapper.newInstance(PersonaModel.class));
	}

	@Override
	public int save(PersonaModel personaModel) {
		String SQL = "INSERT INTO Persona VALUES(?,?,?,?,?,?,?)";
		return jdbcTemplate.update(SQL, new Object[]{personaModel.getNombre(), personaModel.getPrimer_apellido(), personaModel.getSegundo_apellido(),
				personaModel.getTelefono(), personaModel.getEstatus(), personaModel.getFecha_ins(), personaModel.getFecha_upd()});
	}

	@Override
	public int update(PersonaModel personaModel, int id) {
		String SQL = "UPDATE Persona SET nombre=?, primer_apellido=?, segundo_apellido=?, telefono=?, estatus=?, fecha_ins=?, fecha_upd=? WHERE id=?";
		return jdbcTemplate.update(SQL, new Object[]{personaModel.getNombre(), personaModel.getPrimer_apellido(), personaModel.getSegundo_apellido(),
				personaModel.getTelefono(), personaModel.getEstatus(), personaModel.getFecha_ins(), personaModel.getFecha_upd(), personaModel.getId()});
	}

	@Override
	public int deleteById(int id) {
		String SQL = "DELETE FROM Persona WHERE id = ?";
		return jdbcTemplate.update(SQL, id);
	}
}

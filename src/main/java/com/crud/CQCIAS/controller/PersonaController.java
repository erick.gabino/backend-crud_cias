package com.crud.CQCIAS.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.crud.CQCIAS.model.PersonaModel;
import com.crud.CQCIAS.model.ServiceResponse;
import com.crud.CQCIAS.service.IPersonaService;

@RestController
@RequestMapping("api/cqcias")
@CrossOrigin("*")
public class PersonaController {
	@Autowired
	private IPersonaService iPersonaService;
	
	@GetMapping("/list")
	public ResponseEntity<List<PersonaModel>> list(){
		var result = iPersonaService.findAll();
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@PostMapping("/save")
	public ResponseEntity<ServiceResponse> save(@RequestBody PersonaModel personaModel){
		ServiceResponse serviceResponse = new ServiceResponse();
		int result = iPersonaService.save(personaModel);
		if(result == 1) {
			serviceResponse.setMessage("Persona agregada correctamente");
		}
		return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<ServiceResponse> update(@RequestBody PersonaModel personaModel, @PathVariable int id){
		ServiceResponse serviceResponse = new ServiceResponse();
		int result = iPersonaService.update(personaModel, id	);
		if(result == 1) {
			serviceResponse.setMessage("Persona Actualizada Correctamente");
		}
		return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
	}
	@GetMapping("/delete/{id}")
	public ResponseEntity<ServiceResponse> update(@PathVariable int id){
		ServiceResponse serviceResponse = new ServiceResponse();
		int result = iPersonaService.deleteById(id);
		if(result == 1) {
			serviceResponse.setMessage("Persona Borrada Correctamente");
		}
		return new ResponseEntity<>(serviceResponse, HttpStatus.OK);
	}
}

package com.crud.CQCIAS.service;

import java.util.List;
import org.springframework.stereotype.Service;
import com.crud.CQCIAS.model.PersonaModel;

public interface IPersonaService {
	public List<PersonaModel> findAll();
	public int save(PersonaModel personaModel);
	public int update(PersonaModel personaModel, int id);
	public int deleteById(int id);
}

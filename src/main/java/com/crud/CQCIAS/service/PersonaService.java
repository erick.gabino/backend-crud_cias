package com.crud.CQCIAS.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crud.CQCIAS.model.PersonaModel;
import com.crud.CQCIAS.repository.IPersonaRepository;

@Service
public class PersonaService implements IPersonaService{
	
	@Autowired
	private IPersonaRepository iPersonaRepository;

	@Override
	public List<PersonaModel> findAll() {
		List<PersonaModel> list;
		try {
			list = iPersonaRepository.findAll();
		}catch(Exception e){
			throw e;
		}
		return list;
	}

	@Override
	public int save(PersonaModel personaModel) {
		int row;
		try {
			row=iPersonaRepository.save(personaModel);
		}catch(Exception e) {
			throw e;
		}
		return row;
	}

	@Override
	public int update(PersonaModel personaModel, int id) {
		int row;
		try {
			row=iPersonaRepository.update(personaModel, id);
		}catch(Exception e) {
			throw e;
		}
		return row;
	}

	@Override
	public int deleteById(int id) {
		int row;
		try {
			row=iPersonaRepository.deleteById(id);
		}catch(Exception e) {
			throw e;
		}
		return row;
	}

}

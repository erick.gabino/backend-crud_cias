package com.crud.CQCIAS.model;

import java.sql.Date;

import lombok.Data;

@Data
public class PersonaModel {
	int id;
	String nombre;
	String primer_apellido;
	String segundo_apellido;
	String telefono;
	String estatus;
	Date fecha_ins;
	Date fecha_upd;
}

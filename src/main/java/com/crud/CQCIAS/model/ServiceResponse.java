package com.crud.CQCIAS.model;

import lombok.Data;

@Data
public class ServiceResponse {
	Boolean success;
	String message;
}
